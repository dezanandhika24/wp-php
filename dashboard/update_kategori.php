<?php
    session_start();
    if (!isset($_GET["id"])){        
        header('location:kategori.php');
    }else{
        $id = $_GET["id"];
        include('../config/db.php');
        $sql = "SELECT * FROM kategori where id_kategori = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $id);

        $stmt->execute();

        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet" />

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php
        include('sidebar.php');
      ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <nav class="
              navbar navbar-expand navbar-light
              bg-white
              topbar
              mb-4
              static-top
              shadow
            ">
                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard Human Resources Department</h1>
                    </div>

                    <!-- Content Row -->

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Edit Data Kategori</h6>
                        </div>
                        <div class="card-body">
                            <form action="./functions/update_kategori.php" method="POST">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="<?php echo $row["id_kategori"] ?>">
                                    <label for="nama">Nama kategori</label>
                                    <input type="text" name="nama" class="form-control" id="nama"
                                        value="<?php echo $row["nama_kategori"] ?>" placeholder="Masukan Nama kategori">
                                </div>

                                <div class="form-group">
                                    <label for="nama">Tipe Kategori</label>
                                    <select class="form-select name=" name="tipe">

                                        <?php
                    if($row["tipe"]== 1){
                        ?>
                                        <option value='1' selected>Benefit</option>
                                        <option value='2'>Cost</option>
                                        <?php
                    }else{
                    ?>
                                        <option value='1'>Benefit</option>
                                        <option value='2' selected>Cost</option>
                                        <?php
                    }
                    ?>

                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="weight">Weight</label>
                                    <input type="text" name="weight" class="form-control"
                                        value="<?php echo $row["weight"] ?>" id="weight">
                                </div>

                                <br>
                                <button type="submit" class="btn btn-primary">Update Data</button>

                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <?php 
                include('footer.php');
              ?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->


        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
</body>

</html>

<?php
    }
?>