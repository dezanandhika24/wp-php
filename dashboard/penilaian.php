<?php
    session_start();
    include('../config/db.php');
    require_once('./functions/date_helper.php');
    $now_month = 0;
    if(!isset($_GET["bulan"])){
        $now_month = date('n');
    }else{
        $now_month = $_GET["bulan"];
    }
    
    $sql = "SELECT * FROM karyawan";
    
    $result_penilaian = $conn->query($sql);
    
    $sql_kategori ="SELECT id_kategori from kategori order by id_kategori asc";
    $result_kategori = $conn->query($sql_kategori);
    $total_kategori = [];
    $no = 1;

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet" />

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php
        include('sidebar.php');
      ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <nav class="
              navbar navbar-expand navbar-light
              bg-white
              topbar
              mb-4
              static-top
              shadow
            ">
                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Data Penilaian Bulan <?php echo date_to_month($now_month) ?>
                        </h1>

                        <form action="penilaian.php" method="GET">
                            <div> Pilih Bulan :

                                <select name="bulan">
                                    <option selected value='1'>Januari</option>
                                    <option value='2'>Februari</option>
                                    <option value='3'>Maret</option>
                                    <option value='4'>April</option>
                                    <option value='5'>Mei</option>
                                    <option value='6' selected>Juni</option>
                                    <option value='7'>Juli</option>
                                    <option value='8'>Agustus</option>
                                    <option value='9'>September</option>
                                    <option value='10'>Oktober</option>
                                    <option value='11'>November</option>
                                    <option value='12'>Desember</option>
                                </select>
                                <button class="btn btn-primary" type="submit">Pilih</button>

                            </div>
                        </form>
                    </div>

                    <!-- Content Row -->

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Data Karyawan</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama Karyawan</th>
                                            <?php 
                        while($row = $result_kategori->fetch_assoc()){
                            array_push($total_kategori, $row["id_kategori"]);
                            ?>
                                            <th scope="col">C<?php echo $row["id_kategori"] ?></th>
                                            <?php 
                        }
                    
                    ?>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php 
                   while($rows = $result_penilaian->fetch_assoc()){
                        ?>
                                        <tr>
                                            <th><?php echo $no ?></th>
                                            <td><?php echo $rows["nama_karyawan"] ?></td>
                                            <?php
                    $sql_answer = "SELECT * from nilai_karyawan inner join detail_nilai on nilai_karyawan.id_nilai = detail_nilai.id_nilai where (nilai_karyawan.id_karyawan =".$rows["id_karyawan"]." AND MONTH(nilai_karyawan.tanggal_penilaian) = ".$now_month.") and detail_nilai.id_pertanyaan in (".implode(',', $total_kategori).") order by detail_nilai.id_pertanyaan asc";
                    
                    $exe = $conn->query($sql_answer);
                    $tmp_int =0;
                    
                    if($conn->error || $exe->num_rows == 0){
                        for($a=0; $a < count($total_kategori); $a++){
                            echo '<td>0</td>';
                        }
                        ?>
                                            <td><a
                                                    href="tambah_penilaian.php?id=<?php echo $rows["id_karyawan"].";". $now_month?>">Isi
                                                    Penilaian</a>
                                            </td>
                                            <?php
                        $no++;
                        continue;
                    }
                    
                    else{
                    $tmp_array = array();
                    $id_nilai = 0;
                    while($res= $exe->fetch_assoc()){
                        $id_nilai = $res["id_nilai"];
                        array_push($tmp_array, $res["jawaban"]);
                    }

                    $sisa = count($total_kategori) - count($tmp_array);
                    for($a=$sisa; $a >0; $a--){
                        echo '<td>0</td>';
                    }

                    for($a=0; $a < count($tmp_array); $a++){
                        echo '<td>'.$tmp_array[$a].'</td?>';
                                            }
                                            ?>

                                            <td><a class="center"
                                                    href="update_penilaian.php?id=<?php echo $id_nilai ?>">Edit</a>
                                            </td>

                                            <?php

                    ?>
                                            <?php
                }
                $no++;
            }
                ?>

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <?php 
                include('footer.php');
              ?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->


        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
</body>

</html>