<?php
    include('../../config/db.php');
    
    $data = $_POST["jawaban"];
    $id = $_POST["id"];
    
    $sql = "SELECT id_kategori FROM kategori";
    $res = $conn->query($sql);
    
    $a=0;
    while($arr_data = $res->fetch_assoc()){
        $find_s = "SELECT * FROM detail_nilai where id_nilai= ".$id." AND id_pertanyaan=".$arr_data["id_kategori"];
        
        $q = $conn->query($find_s);

        if($q->num_rows != 0){
            $s = "UPDATE detail_nilai SET jawaban = ? where id_nilai =? AND id_pertanyaan =?";
            $st = $conn->prepare($s);

            $st->bind_param("sss", $data[$a], $id, $arr_data["id_kategori"]);
            $st->execute();
        }else{
            $s = "INSERT INTO detail_nilai (id_pertanyaan,jawaban,id_nilai) VALUES(?,?,?)";
            $st = $conn->prepare($s);

            $st->bind_param("sss", $arr_data["id_kategori"], $data[$a], $id);
            $st->execute();
        }

        $a++;
    }
    
    echo "<script>
             alert('Sukses!');
             window.location.href='../penilaian.php';
             </script>";

?>