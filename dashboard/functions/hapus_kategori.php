<?php
    if (isset($_GET["id"])){
        include('../../config/db.php');
        
        $param = $_GET["id"];
        $sql = "DELETE FROM kategori where id_kategori = ?";
        
        if($stmt = $conn->prepare($sql)){
            $stmt->bind_param("s", $param);
    
            if($stmt->execute()){
                echo "<script>
                alert('Sukses!');
                window.location.href='../';
                </script>";
            }else{
                echo $stmt->error;
            } 
       }else{
           echo $conn->error;
       }
        
    }else{
        header('location:../');
    }

?>