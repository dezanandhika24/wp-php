<?php
    if(!isset($_POST["id"]) && !isset($_POST["alamat"]) && !isset($_POST["nama"]) && !isset($_POST["tanggal"])){
        echo "<script>
        alert('Mohon isi semua data!');
        window.location.href='../';
        </script>";
    }else{
        include('../../config/db.php');
        $nama = $_POST["nama"];
        $tanggal_lahir = $_POST["tanggal"];
        $alamat = $_POST["alamat"];
        $id = $_POST["id"];
        $omset = $_POST["omset"];
        $jenis_kelamin = $_POST["jenis_kelamin"];
        $nik = $_POST["nik"];
        $grade = $_POST["grade"];
        $divisi = $_POST["divisi"];

        $sql = "UPDATE karyawan SET nama_karyawan =?, tanggal_lahir =?, alamat=?, nik=?, target_omset=?, jenis_kelamin=?, grade_sales=?, divisi=? where id_karyawan = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sssssssss", $nama, $tanggal_lahir, $alamat, $nik, $omset, $jenis_kelamin, $grade, $divisi, $id);
        if($stmt->execute()){
            echo "<script>
            alert('Sukses!');
            window.location.href='../';
            </script>";
        }else{
            echo $stmt->error;
        } 
    }

?>