<?php
include('../../config/db.php');

    if (!isset($_POST["nama"]) || !isset($_POST["weight"])){
        
        echo "<script>
        alert('Mohon isi semua data!');
        window.location.href='../kategori.php';
        </script>";
        
    }
    
   $nama = $_POST["nama"];
   $weight = $_POST["weight"];
   $tipe = $_POST["tipe"];

   if ($weight > 5 ){
    echo "<script>
    alert('Weight harus kurang dari atau sama dengan 5!');
    window.location.href='../kategori.php';
    </script>";
    
   }else{

    $sql = "insert into kategori(nama_kategori, weight, tipe) values(?,?,?)";
    if($stmt = $conn->prepare($sql)){
         $stmt->bind_param("sss", $nama, $weight, $tipe);
 
         if($stmt->execute()){
             echo "<script>
             alert('Sukses!');
             window.location.href='../kategori.php';
             </script>";
         }else{
             echo $stmt->error;
         } 
    }else{
        echo $conn->error;
    }
   }


?>