<?php
include('../../config/db.php');

    if (!isset($_POST["nama"]) || !isset($_POST["tanggal"]) || !isset($_POST["alamat"])){
        
        echo "<script>
        alert('Mohon isi semua data!');
        window.location.href='../';
        </script>";
        
    }
    
   $nama = $_POST["nama"];
   $tanggal_lahir = $_POST["tanggal"];
   $alamat = $_POST["alamat"];
   $omset = $_POST["omset"];
   $jenis_kelamin = $_POST["jenis_kelamin"];
   $nik = $_POST["nik"];
   $grade = $_POST["grade"];
   $divisi = $_POST["divisi"];

   $sql = "insert into karyawan(nama_karyawan, alamat, tanggal_lahir, target_omset, jenis_kelamin, nik, grade_sales, divisi) values(?,?,?,?,?,?,?,?)";
   if($stmt = $conn->prepare($sql)){
        $stmt->bind_param("ssssssss", $nama, $alamat, $tanggal_lahir, $omset, $jenis_kelamin, $nik, $grade, $divisi);

        if($stmt->execute()){
            echo "<script>
            alert('Sukses!');
            window.location.href='../';
            </script>";
        }else{
            echo $stmt->error;
        } 
   }else{
       echo $conn->error;
   }

?>