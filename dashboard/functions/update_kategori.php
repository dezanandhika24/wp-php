<?php
    if(!isset($_POST["id"]) || !isset($_POST["nama"]) || !isset($_POST["weight"])){
        echo "<script>
        alert('Mohon isi semua data!');
        window.location.href='../kategori.php';
        </script>";
    }else{
        include('../../config/db.php');
        $nama = $_POST["nama"];
        $weight = $_POST["weight"];
        $id = $_POST["id"];
        $tipe = $_POST['tipe'];

        $sql = "UPDATE kategori SET nama_kategori =?, weight =?, tipe=? where id_kategori = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("ssss", $nama, $weight, $tipe,$id);
        if($stmt->execute()){
            echo "<script>
            alert('Sukses!');
            window.location.href='../kategori.php';
            </script>";
        }else{
            echo $stmt->error;
        } 
    }

?>