<?php
    session_start();
    include('../config/db.php');
    $sql = "select * from karyawan";
    $result = $conn->query($sql);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet" />

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php
        include('sidebar.php');
      ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Data Karyawan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="./functions/tambah_karyawan.php" method="POST">
                                <div class="form-group">
                                    <label for="nama">Nama Karyawan</label>
                                    <input type="text" name="nama" class="form-control" id="nama"
                                        placeholder="Masukan Nama Karyawan">
                                </div>

                                <div class="form-group">
                                    <label for="nama">NIK Karyawan</label>
                                    <input type="text" name="nik" class="form-control" id="nik"
                                        placeholder="Masukan NIK Karyawan">
                                </div>

                                <div class="form-group">
                                    <label for="nama">Divisi Karyawan</label>
                                    <input type="text" name="divisi" class="form-control" id="Divisi"
                                        placeholder="Masukan Divisi Karyawan">
                                </div>

                                <div class="form-group">
                                    <label for="omset">Target Omset Karyawan</label>
                                    <input type="text" name="omset" class="form-control" id="omset"
                                        placeholder="Masukan Omset Karyawan">
                                </div>

                                <div class="form-group">
                                    <label for="nama">Grade Karyawan</label>
                                    <input type="text" name="grade" class="form-control" id="grade"
                                        placeholder="Masukan Grade Karyawan">
                                </div>

                                <div class="form-group">
                                    <label for="tanggal">Tanggal Lahir</label>
                                    <input type="date" name="tanggal" class="form-control" id="tanggal"
                                        placeholder="Tanggal Lahir">
                                </div>

                                <div class="form-group">
                                    <label for="nama">Jenis Kelamin</label>
                                    <select class="form-select name=" name="jenis_kelamin">
                                        <option value='1' selected>Laki-laki</option>
                                        <option value='2'>Perempuan</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Alamat</label>
                                    <textarea class="form-control" name="alamat" id="exampleFormControlTextarea1"
                                        rows="3"></textarea>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary">Submit</button>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <nav class="
              navbar navbar-expand navbar-light
              bg-white
              topbar
              mb-4
              static-top
              shadow
            ">
                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard Human Resources Department</h1>


                        <a href="#" class="
                  d-none d-sm-inline-block
                  btn btn-sm btn-primary
                  shadow-sm
                " data-toggle="modal" data-target="#exampleModal"><i class="fas fa-plus fa-sm text-white-50"></i>
                            Tambah Karyawan</a>
                    </div>

                    <!-- Content Row -->

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Data Karyawan</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIK</th>
                                            <th>Nama</th>
                                            <th>Divisi</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>

                                        <?php 

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
?>

                                        <tr>
                                            <th scope="row"><?php echo $row["id_karyawan"] ?></th>
                                            <td><?php echo $row["nik"] ?></td>
                                            <td><?php echo $row["nama_karyawan"] ?></td>
                                            <td><?php echo $row["divisi"] ?></td>
                                            <td><?php echo $row["tanggal_lahir"] ?></td>
                                            <td><a href="update.php?id=<?php echo $row["id_karyawan"] ?>">Edit</a> |
                                                <a href="functions/hapus_karyawan.php?id=<?php echo $row["id_karyawan"] ?>"
                                                    onClick="return  confirm('do you want to delete Y/N')">Hapus </a>
                                            </td>
                                        </tr>


                                        <?php 
    }
}else {
        echo "0 results";
    }
?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <?php 
                include('footer.php');
              ?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->


        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
</body>

</html>