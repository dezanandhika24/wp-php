<?php
    session_start();
    if (!isset($_GET["id"])){        
        header('location:index.php');
    }else{
        $id = $_GET["id"];
        include('../config/db.php');
        $sql = "SELECT * FROM karyawan inner join nilai_karyawan on nilai_karyawan.id_karyawan = karyawan.id_karyawan 
        inner join detail_nilai on nilai_karyawan.id_karyawan = detail_nilai.id_nilai where nilai_karyawan.id_nilai = ?";
        $stmt = $conn->prepare($sql);

        $stmt->bind_param("s", $id);
        $stmt->execute();

        $result = $stmt->get_result();
        $row = $result->fetch_assoc();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet" />

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php
        include('sidebar.php');
      ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <nav class="
              navbar navbar-expand navbar-light
              bg-white
              topbar
              mb-4
              static-top
              shadow
            ">
                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Update Penilaian Karyawan
                        </h1>


                    </div>

                    <!-- Content Row -->

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Data Penilaian</h6>
                        </div>
                        <div class="card-body">
                            <form action="./functions/update_penilaian.php" method="POST">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="<?php echo $id ?>">
                                    <label for="nama">Nama Karyawan</label>
                                    <input type="text" name="nama" class="form-control" disabled id="nama"
                                        value="<?php echo $row["nama_karyawan"] ?>" placeholder="Masukan Nama Karyawan">
                                </div>

                                <div class="form-group">
                                    <label for="tanggal">Tanggal Penilaian</label>
                                    <input type="date" name="tanggal" class="form-control" disabled
                                        value="<?php echo $row["tanggal_penilaian"] ?>" id="tanggal"
                                        placeholder="Tanggal Penilaian">
                                </div>

                                <?php
                $s = "SELECT * FROM kategori";
                $exe = $conn->query($s);

                while($row = $exe->fetch_assoc()){
                    ?>
                                <div class="form-group">
                                    <label>Nilai <?php echo $row["nama_kategori"] ?></label>


                                    <?php 
                    $sql_answer = "SELECT * FROM nilai_karyawan inner join detail_nilai on nilai_karyawan.id_karyawan = detail_nilai.id_nilai where nilai_karyawan.id_nilai = ? AND detail_nilai.id_pertanyaan = ?";
                    $stmt_answer = $conn->prepare($sql_answer);
                    $stmt_answer->bind_param("ss", $id, $row["id_kategori"]);

                    $stmt_answer->execute();
            
                    $result = $stmt_answer->get_result();
                    
                    if($result->num_rows == 0){
                        ?>
                                    <input type="text" name="jawaban[]" value=0 class="form-control" id="jawaban"
                                        placeholder="Masukan Nilai <?php echo $row["nama_kategori"] ?>">
                                    <?php
                    }else{
                        $data = $result->fetch_assoc();
                        ?>
                                    <input type="text" name="jawaban[]" class="form-control" id="jawaban"
                                        value="<?php echo $data["jawaban"] ?>">
                                    <?php
                    }
                    
                ?>
                                </div>

                                <?php
            }
            ?>

                                <br>
                                <button type="submit" class="btn btn-primary">Update Data</button>

                            </form>
                        </div>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                <?php 
                include('footer.php');
              ?>
                <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
        </div>
        <!-- End of Page Wrapper -->


        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
</body>

</html>

<?php
    }
?>