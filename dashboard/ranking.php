<?php
    session_start();
    require_once('./functions/date_helper.php');
    require('../config/db.php');
    $now_month = 0;
    if(!isset($_GET["bulan"])){
        $now_month = date('n');
    }else{
        $now_month = $_GET["bulan"];
    }
    
    $sql = "SELECT * FROM karyawan";
    
    $result_penilaian = $conn->query($sql);
    
    $sq = "SELECT id_kategori, weight ,tipe From kategori order by id_kategori asc";
    $ex = $conn->query($sq);
    $nilai = array();
    $id_kategori = array();
    $status_kategori = array();
    $vector_s = array();

    $sq_total = "SELECT SUM(weight) as total From kategori order by id_kategori asc";
    $exs = $conn->query($sq_total);
    $data_total = $exs->fetch_assoc();

    $total = $data_total["total"];
    while($data = $ex->fetch_assoc()){
        if($data["tipe"] == 1){
            array_push($nilai, $data["weight"]);
        }else{
            array_push($nilai, $data["weight"]* -1);
        }
        array_push($id_kategori, $data["id_kategori"]);
    }
    
    $weight = array();
    for($a =0; $a< count($nilai); $a++){
        $tmp = $nilai[$a] / $total;
        $weight[$id_kategori[$a]] = round($tmp,4);
    }
       
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet" />

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Page Wrapper -->
    <div id="wrapper">
        <!-- Sidebar -->
        <?php
        include('sidebar.php');
      ?>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="./functions/tambah_kategori.php" method="POST">
                                <div class="form-group">
                                    <label for="nama">Nama kategori</label>
                                    <input type="text" name="nama" class="form-control" id="nama"
                                        placeholder="Masukan Nama kategori">
                                </div>

                                <div class="form-group">
                                    <label for="weight">Weight</label>
                                    <input type="text" name="weight" class="form-control" id="weight"
                                        placeholder="Masukan weight kategori">
                                </div>

                                <div class="form-group">
                                    <label for="nama">Tipe Kategori</label>
                                    <select class="form-select name=" name="tipe">
                                        <option value='1' selected>Benefit</option>
                                        <option value='2'>Cost</option>
                                    </select>
                                </div>

                                <br>
                                <button type="submit" class="btn btn-primary">Submit</button>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Content -->
            <div id="content">
                <!-- Topbar -->
                <nav class="
              navbar navbar-expand navbar-light
              bg-white
              topbar
              mb-4
              static-top
              shadow
            ">
                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Dashboard Human Resources Department</h1>
                        <a href="#" class="
                  d-none d-sm-inline-block
                  btn btn-sm btn-primary
                  shadow-sm
                " data-toggle="modal" data-target="#exampleModal"><i class="fas fa-download fa-sm text-white-50"></i>
                            Generate Report</a>
                    </div>

                    <!-- Content Row -->

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Nilai Vektor S</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama Karyawan</th>
                                            <th scope="col">Nilai Vektor S</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php 
                    $no_s =1;
                    while($rows = $result_penilaian->fetch_assoc()){
                        ?>
                                        <?php
                $sql_s = "SELECT * from karyawan left join nilai_karyawan where (nilai_karyawan.id_karyawan =".$rows["id_karyawan"]." AND MONTH(nilai_karyawan.tanggal_penilaian) = ".$now_month.")";
            ?>
                                        <tr>
                                            <th scope="row"><?php echo $no_s ?></th>
                                            <td><?php echo $rows["nama_karyawan"] ?></td>


                                            <?php
                        $sql_h_s = "SELECT * FROM nilai_karyawan inner join detail_nilai on nilai_karyawan.id_nilai = detail_nilai.id_nilai
                        where nilai_karyawan.id_karyawan = ".$rows["id_karyawan"]." AND MONTH(nilai_karyawan.tanggal_penilaian) = ".$now_month. " 
                        order by detail_nilai.id_pertanyaan asc";
                        
                        $exec = $conn->query($sql_h_s);
                        if($exec->num_rows == 0 || $conn->error){
                            echo '<td>0</td>';
                        }else{
                            $cur_vector_s=0;
                            $tmp_total = 0;
                            $now_id=0;
                            while($row = $exec->fetch_assoc()){
                                $now_id = $row["id_nilai"];
                                if($tmp_total==0){
                                    $tmp_total = pow($row["jawaban"], $weight[$row["id_pertanyaan"]]);
                                }else{
                                    $ey = pow($row["jawaban"], $weight[$row["id_pertanyaan"]]);
                                    $tmp_total = $ey * $tmp_total;
                                    $tmp_total = round($tmp_total, 4);
                                }
                            }
                            $vector_s[$now_id] = $tmp_total;
                            echo '<td>'.$tmp_total.'</td>';
                        }

                    ?>
                                        </tr>

                                        <?php
                $no_s++;
                    }
                    $total_vector_s = 0;
                    $vector_values = array_values($vector_s);
                    for($a=0; $a< count($vector_values); $a++){
                        $total_vector_s = $total_vector_s + $vector_values[$a];
                    }
                ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>




                    </div>

                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Nilai Vektor V</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">Nama Karyawan</th>
                                            <th scope="col">Nilai Vektor V</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php 
                    $no_s =1;
                    $ex2 = $conn->query($sql);
                    while($rows = $ex2->fetch_assoc()){
                        ?>
                                        <?php
                $sql_s = "SELECT * from karyawan left join nilai_karyawan where (nilai_karyawan.id_karyawan =".$rows["id_karyawan"]." AND MONTH(nilai_karyawan.tanggal_penilaian) = ".$now_month.")";
            ?>
                                        <tr>
                                            <th scope="row"><?php echo $no_s ?></th>
                                            <td><?php echo $rows["nama_karyawan"] ?></td>


                                            <?php
                        $sql_h_s = "SELECT * FROM nilai_karyawan
                        where nilai_karyawan.id_karyawan = ".$rows["id_karyawan"]."
                        AND MONTH(nilai_karyawan.tanggal_penilaian) = ".$now_month;
                        
                        $exec = $conn->query($sql_h_s);
                        if($exec->num_rows == 0 || $conn->error){
                            echo '<td>0</td>';
                        }else{
                           while($row_s = $exec->fetch_assoc()){
                                $vector_v = $vector_s[$row_s["id_nilai"]] / $total_vector_s;
                                $vector_v = round($vector_v, 4);
                                echo '<td>'.$vector_v.'</td>';
                           }
                        }

                    ?>
                                        </tr>

                                        <?php
                $no_s++;
                    }
                ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.container-fluid -->
                    </div>
                    <!-- End of Main Content -->

                    <!-- Footer -->
                    <?php 
                include('footer.php');
              ?>
                    <!-- End of Footer -->
                </div>
                <!-- End of Content Wrapper -->
            </div>
            <!-- End of Page Wrapper -->


            <!-- Bootstrap core JavaScript-->
            <script src="vendor/jquery/jquery.min.js"></script>
            <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

            <!-- Core plugin JavaScript-->
            <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

            <!-- Custom scripts for all pages-->
            <script src="js/sb-admin-2.min.js"></script>
</body>

</html>