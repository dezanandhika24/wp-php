<?php
    $role = $_SESSION["role"];
?>

<ul class="navbar-nav bg-gradient-warning sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon">
            <img src="https://www.courts.co.id/frontend/img/logo.png" class="logo" width="130px">
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0" />
    <?php
        if($role != "Sales Assistant"){
    ?>
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Karyawan</span></a>
    </li>

    <?php 
        if ($role != "Manajer"){
    ?>
    <li class="nav-item">
        <a class="nav-link" href="kategori.php">
            <i class="fas fa-fw fa-folder"></i>
            <span>Kategori</span></a>
    </li>
    <?php
        }
    if($role != "HRD"){
        ?>
    <!-- Nav Item - Tables -->
    <li class="nav-item">
        <a class="nav-link" href="penilaian.php">
            <i class="fas fa-fw fa-table"></i>
            <span>Penilaian</span></a>
    </li>
    <?php
    }
        }

    ?>
    <li class="nav-item">
        <a class="nav-link" href="ranking.php">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Rangking Karyawan</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="./functions/logout.php">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Logout</span></a>
    </li>



</ul>